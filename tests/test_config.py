import tempfile
from pathlib import PosixPath
from unittest import TestCase

from schedbot.config import SchedbotConfig


class TestSchedbotConfig(TestCase):

    def test_config_file(self):
        config_file = b'''
token: testtoken
channel_id: 1234567890
users_file: test/users.json
schedule_file: test/schedule.json
'''

        with tempfile.NamedTemporaryFile() as tmp:
            tmp.write(config_file)
            tmp.flush()
            tmp.seek(0)
            config = SchedbotConfig(['--config', tmp.name])

            self.assertEqual(config['token'], 'testtoken')
            self.assertEqual(config['channel_id'], 1234567890)
            self.assertEqual(config['users_file'], 'test/users.json')
            self.assertEqual(config['schedule_file'], 'test/schedule.json')

    def test_cli_args(self):
        with tempfile.NamedTemporaryFile() as tmp:
            config = SchedbotConfig([
                '--verbose',
                '--dry-run',
                '--token', 'faketoken',
                '--prefix', '@',
                '--channel-id', '12345678',
                '--config', tmp.name,
            ])

            self.assertEqual(config['verbose'], True)
            self.assertEqual(config['dry_run'], True)
            self.assertEqual(config['token'], 'faketoken')
            self.assertEqual(config['prefix'], '@')
            self.assertEqual(config['channel_id'], 12345678)
            self.assertEqual(config['config'], PosixPath(tmp.name))

    def test_config_precedence(self):
        config_file = b'''
token: token_from_file
channel_id: 824
users_file: test/users.json
schedule_file: test/schedule.json
prefix: '-'
verbose: true
'''

        with tempfile.NamedTemporaryFile() as tmp:
            tmp.write(config_file)
            tmp.flush()
            config = SchedbotConfig([
                '--token', 'token_from_cli',
                '--prefix', '@',
                '--channel-id', '541',
                '--config', tmp.name,
                '--dry-run'
            ])

        self.assertEqual(config['verbose'], True)
        self.assertEqual(config['dry_run'], True)
        self.assertEqual(config['token'], 'token_from_cli')
        self.assertEqual(config['prefix'], '@')
        self.assertEqual(config['channel_id'], 541)
        self.assertEqual(config['config'], PosixPath(tmp.name))
