import argparse
import pathlib

import yaml
from yaml.scanner import ScannerError

from schedbot.log import logger


class SchedbotConfig(dict):

    def __init__(self, args):

        super().__init__()

        defaults = {
            'prefix': '!',
            'users_file': './users.json',
            'schedule_file': './schedule.json',
        }

        self._parse_cli_args(args)
        self._parse_file(self['config'])

        for key in defaults:
            if key not in self or self[key] is None:
                self[key] = defaults[key]

    def _parse_cli_args(self, args: list[str]):
        parser = argparse.ArgumentParser()

        # CLI Options
        parser.add_argument('-v', '--verbose', action='store_true', default=None)
        parser.add_argument('-d', '--dry-run', action='store_true', default=None)
        parser.add_argument('-c', '--config', type=pathlib.Path, default=pathlib.Path('./schedbot.yaml'))

        # General Options
        parser.add_argument('-p', '--prefix', type=str)
        parser.add_argument('-t', '--token', type=str)
        parser.add_argument('-i', '--channel-id', type=int)

        parsed_args = parser.parse_args(args)

        self.update({
            'verbose': parsed_args.verbose,
            'dry_run': parsed_args.dry_run,
            'token': parsed_args.token,
            'prefix': parsed_args.prefix,
            'config': parsed_args.config,
            'channel_id': parsed_args.channel_id,
        })

    def _parse_file(self, config_file_path: str):
        try:
            with open(config_file_path, 'r') as file:
                config_file = yaml.load(file, Loader=yaml.FullLoader)
                if config_file is None:
                    return
                for key in config_file:
                    if key not in self or self[key] is None:
                        self[key] = config_file[key]
                logger.debug("Config file loaded")
        except ScannerError:
            logger.error("Invalid config format")
        except FileNotFoundError:
            logger.error("Could not locate config file")
