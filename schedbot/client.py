from typing import Optional

import discord
from discord.ext import commands

from schedbot.cmd_base import BaseCommand
from schedbot.cog_base import BaseCog
from schedbot.log import logger


class SchedBot(commands.Bot):

    def __init__(self, channel_id: int, command_prefix: str):

        super().__init__(command_prefix=command_prefix, intents=discord.Intents.default())
        self.channel_id: int = channel_id
        self.channel: Optional[discord.TextChannel] = None

    async def on_ready(self):
        logger.info(f"Logged on as {self.user}")
        self.channel = self.get_channel(self.channel_id)
        logger.debug(f"Assigned to channel '{self.channel}' (id: {self.channel_id})")

    async def on_message(self, message: discord.Message):
        # only process cogs sent to the configured channel
        if message.channel.id == self.channel_id:
            await self.process_commands(message)

    async def send_message(self, content: str):
        logger.debug(f"Trying to send message '{content}'")
        if self.channel is None:
            logger.error(f"No channel found")
            return

        await self.channel.send(content=content)

    async def send_embed(self, embed: discord.Embed):
        logger.debug(f"Trying to send embed '{embed.title}'")
        if self.channel is None:
            logger.error(f"No channel found")
            return

        await self.channel.send(embed=embed)

    def add_cog(self, cog: BaseCog):
        logger.debug(f"Adding cog '{cog.qualified_name}'")
        super().add_cog(cog)
        cog.after_added()

    def add_command(self, command: BaseCommand):
        logger.debug(f"Adding command '{command.qualified_name}'")
        super().add_command(command)

    def run(self, *args, **kwargs):
        logger.debug(f"Connecting...")
        super().run(*args, **kwargs)
