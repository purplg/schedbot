import tempfile
from unittest import TestCase

from schedbot.cog_timezone import SetCommand
from schedbot.data import UserData, UserEncoder, FileWriter

class MockAuthor:
    def __init__(self, id: int, name: str):
        self.id = id
        self.name = name


class MockContext:
    def __init__(self, author_id: int, author_name: str):
        self.author = MockAuthor(author_id, author_name)


class TestSetCommand(TestCase):

    def test_new_user_invalid_timezone(self):
        with tempfile.NamedTemporaryFile() as tmp:
            cmd = SetCommand(UserData(), FileWriter(tmp.name, UserEncoder))
            embed = cmd.execute(None, 'BadTimezone')  # type: ignore
            self.assertEqual(embed.title, "Registration failed! :(")

    def test_new_user_valid_timezone(self):
        with tempfile.NamedTemporaryFile() as tmp:
            cmd = SetCommand(UserData(), FileWriter(tmp.name, UserEncoder))
            embed = cmd.execute(MockContext(1, 'UserWithId1'), 'America/Chicago')  # type: ignore

            self.assertEqual(embed.title, "Registration successful! :)")
            self.assertEqual(embed.fields[0].name, 'User')
            self.assertEqual(embed.fields[0].value, 'UserWithId1')
            self.assertEqual(embed.fields[1].name, 'Timezone')
            self.assertEqual(embed.fields[1].value, 'America/Chicago')

    def test_fuzzy_timezone_match(self):
        cmd = SetCommand(None, None)  # type: ignore

        embed = cmd.execute(None, 'Asia/Kuwaut')  # type: ignore
        self.assertEqual(embed.description, "Unknown timezone. Did you mean \"Asia/Kuwait\"?")
