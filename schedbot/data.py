import json
from json.encoder import JSONEncoder
from typing import Dict, List, Optional, Type

from schedbot.log import logger


class UnknownUserException(Exception):
    pass


class User(dict):
    """ Model for mapping and serializing (via dict) a Discord user ID to their Timezone """

    def __init__(self, user_id: int, name: str, tz: str):
        self.id = user_id
        self.name = name
        self.tz = tz
        dict.__init__(self, id=user_id, name=name, tz=tz)  # For serialization as a dict


class UserData:
    """ Data structure for storing all of the registered users data """

    def __init__(self, users: List[Dict] = None):
        """ Accepts a list of dictionary objects containing User data """

        self._users = {}
        self._zones = {}

        if users is not None: [self.add_user(user['id'], user['name'], user['tz']) for user in users]

    def get_user(self, user_id: int) -> Optional[User]:
        if user_id not in self._users.keys():
            return None
        return self._users[user_id]

    def remove_user(self, user_id: int):
        user = self._users[user_id]
        if user is None:
            return

        for i, tzUser in enumerate(self._zones[user.tz]):
            if tzUser.id == user_id:
                del self._zones[user.tz][i]
                if len(self._zones[user.tz]) == 0:
                    del self._zones[user.tz]
                break

        del self._users[user_id]

    def get_timezones(self) -> List[str]:
        return list(self._zones.keys())

    def get_users(self) -> List[dict]:
        return list(self._users.values())

    def get_users_in_timezone(self, tz_name: str) -> List[User]:
        if tz_name not in self._zones.keys():
            return []
        return self._zones[tz_name]

    def add_user(self, id: int, name: str, tz: str):
        logger.debug(f"Adding user - id: {id}, name: {name}, tz: {tz}")

        if id in self._users.keys():
            self.remove_user(id)

        tz_user = User(id, name, tz)
        self._users[id] = tz_user
        if tz not in self._zones:
            self._zones[tz] = []
        self._zones[tz].append(tz_user)

    @staticmethod
    def new_from_file(path: str):
        try:
            with open(path, 'r') as file:
                user_data = UserData()
                j = json.load(file)
                for user in j:
                    user_data.add_user(user['id'], user['name'], user['tz'])
            logger.debug("Users loaded")
            return user_data
        except FileNotFoundError:
            logger.warn("users json file not found. Creating empty one")
            return UserData()


class UserEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UserData):
            return obj.get_users()
        return json.JSONEncoder.default(self, obj)


class EventData(dict):
    """ Model representing a single upcoming event """

    def __init__(self, user_id: int, time: float, title: str):
        super().__init__({
            'user_id': user_id,
            'time': time,
            'title': title,
        })

    @property
    def id(self):
        return self['id']

    @property
    def user_id(self):
        return self['user_id']

    @property
    def time(self):
        return self['time']

    @property
    def title(self):
        return self['title']


class ScheduleData:
    """ Data structure for storing all of the upcoming events """

    def __init__(self, events: List = None):
        self._events: List = [] if events is None else events
        self._events = [EventData(event['user_id'], event['time'], event['title']) for event in self._events]
        self._events.sort(key=lambda event: event.time)

    def add_event(self, user_id: int, time: float, title: str) -> EventData:
        event_data = EventData(user_id, time, title)
        self._events.append(event_data)
        self._events.sort(key=lambda event: event.time)
        return event_data

    def peek(self) -> Optional[EventData]:
        return self._events[0] if len(self._events) > 0 else None

    def pop(self) -> Optional[EventData]:
        return self._events[0] if len(self._events) > 0 else None

    def remove(self, event_data: EventData) -> None:
        self._events.remove(event_data)

    def __iter__(self):
        return iter(self._events)

    @staticmethod
    def new_from_file(path: str):
        schedule_data = ScheduleData()
        try:
            with open(path, 'r') as file:
                j = json.load(file)
                for event in j:
                    try:
                        schedule_data.add_event(event['user_id'], event['time'], event['title'])
                    except UnknownUserException:
                        logger.warn(f"Unknown user with id {event['user_id']}. Skipping")
        except FileNotFoundError:
            logger.warn("schedule json file not found. Creating empty one")
            return ScheduleData()
        logger.debug("Schedule data loaded")
        return schedule_data


class ScheduleEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, EventData):
            return {'user_id': obj.user_id, 'time': obj.time, 'title': obj.title, }
        elif isinstance(obj, ScheduleData):
            return obj._events
        return json.JSONEncoder.default(self, obj)


class FileWriter:
    """ Generic write data to a file """

    def __init__(self, path: str, encoder: Type[JSONEncoder]):
        self.path = path
        self.encoder = encoder

    def write(self, obj):
        with open(self.path, 'w') as file:
            json.dump(obj, file, cls=self.encoder)
