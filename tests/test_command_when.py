from datetime import datetime
from unittest import TestCase

from freezegun import freeze_time

from schedbot.cog_timezone import UserData, WhenCommand


class MockAuthor:
    def __init__(self, id: int, name: str):
        self.id = id
        self.name = name


class MockContext:
    def __init__(self, author_id: int, author_name: str):
        self.author = MockAuthor(author_id, author_name)


class TestWhenCommand(TestCase):

    @freeze_time(datetime(year=2019, month=2, day=10, hour=6, minute=59))
    def test_specified_time(self):
        user_data = UserData()
        user_data.add_user(1, "UserWithId1", "America/Chicago")
        user_data.add_user(2, "UserWithId2", "America/New_York")

        cmd = WhenCommand(user_data)  # type: ignore
        embed = cmd.execute(MockContext(1, 'UserWithId1'), "10:00")  # type: ignore

        self.assertEqual(embed.footer.text, "Your time: 10:00 CST")
        self.assertEqual(embed.fields[0].value, "Sun 10:00 CST")
        self.assertEqual(embed.fields[1].value, "Sun 11:00 EST")

    @freeze_time(datetime(year=2020, month=6, day=20, hour=19, minute=18))
    def test_unspecified_time(self):
        user_data = UserData()
        user_data.add_user(1, 'UserWithId1', 'America/Chicago')
        user_data.add_user(2, 'UserWithId2', 'America/New_York')

        cmd = WhenCommand(user_data)  # type: ignore

        embed = cmd.execute(MockContext(1, 'UserWithId1'))  # type: ignore
        self.assertEqual(embed.footer.text, "Your time: 14:18 CDT")
        self.assertEqual(embed.fields[0].value, "Sat 14:18 CDT")
        self.assertEqual(embed.fields[1].value, "Sat 15:18 EDT")

        embed = cmd.execute(MockContext(2, 'UserWithId2'))  # type: ignore
        self.assertEqual(embed.footer.text, "Your time: 15:18 EDT")
        self.assertEqual(embed.fields[0].value, "Sat 14:18 CDT")
        self.assertEqual(embed.fields[1].value, "Sat 15:18 EDT")

    def test_user_enumeration(self):
        user_data = UserData()
        user_data.add_user(1, "UserWithId1", "America/New_York")
        user_data.add_user(2, "UserWithId2", "America/Chicago")
        user_data.add_user(3, "UserWithId3", "America/New_York")
        user_data.add_user(4, "UserWithId4", "America/New_York")

        cmd = WhenCommand(user_data)  # type: ignore
        embed = cmd.execute(MockContext(1, 'UserWithId1'))  # type: ignore

        self.assertEqual(embed.fields[0].name, "UserWithId1, UserWithId3, UserWithId4")
