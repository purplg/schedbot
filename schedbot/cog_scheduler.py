import uuid
from typing import Callable, List
from datetime import datetime

import discord
import pytz
from discord.ext import commands, tasks

from schedbot.client import SchedBot
from schedbot.cmd_base import BaseCommand
from schedbot.cog_base import BaseCog
from schedbot.cog_timezone import UserData
from schedbot.embeds import embed_info, embed_failure, embed_success
from schedbot.log import logger
from schedbot.data import EventData, ScheduleData, FileWriter, UnknownUserException



class EventCog(BaseCog):

    def __init__(self, bot: SchedBot, event_data: EventData, callback: Callable):
        """
        time: UTC time the event is scheduled for
        callback: The function called when the event triggers
        """

        self._bot: SchedBot = bot
        self.name = str(uuid.uuid4())
        self._event_data = event_data
        self._skip_trigger = True  # To skip the first iteration of the loop, which triggers instantly
        self._task = None
        self._callback: Callable = callback

    @property
    def event_data(self):
        return self._event_data

    def start(self):
        logger.debug("Starting loop...")
        delta_time = max(0.0, self._event_data.time - datetime.now().timestamp())
        self._wait_for_event.change_interval(seconds=delta_time)
        self._task = self._wait_for_event.start()

    @tasks.loop(count=2)
    async def _wait_for_event(self):
        """
        The main loop. Only runs when there is an upcoming event
        Need to skip first loop because it triggers instantly on `start()`
        """

        if self._skip_trigger:
            self._skip_trigger = False
            return
        await self._on_trigger()

    async def _on_trigger(self):
        """ Called when a schedule event occurs """

        logger.debug(f"Event triggered - {self._event_data.title}")
        self._callback(self)
        await self._bot.send_message(f"Event triggered! {self._event_data.title}")

    @_wait_for_event.before_loop
    async def _wait_for_ready(self):
        if not self._bot.is_ready:
            logger.debug("Waiting for bot to start...")
            await self._bot.wait_until_ready()


class Scheduler(BaseCog):
    """ Maintains an chronologically ordered collection of `Event`'s """

    def __init__(self, bot: SchedBot, user_data: UserData, writer: FileWriter, schedule_data: ScheduleData = None):
        self._bot: SchedBot = bot
        self._user_data = user_data
        self._schedule_data: ScheduleData = ScheduleData()
        self._writer = writer
        self._event_cogs: List[EventCog] = []

        # If schedule_data was provided, populate the data by creating all the `EventCog`s for it
        if schedule_data is not None:
            [self.add_event(event.user_id, event.time, event.title) for event in schedule_data]

    def after_added(self):
        self._bot.add_command(ListCommand(self))
        self._bot.add_command(MeetCommand(self))

    def add_event(self, user_id: int, requested_timestamp: float, title: str):
        """
        Adds an event to the schedule

        user_id: The Discord user ID of the user who scheduled the event
        time: A timestamp value in the Users' registered timezone
        title: The title of the event passed by the user
        """

        try:
            user = self._user_data.get_user(user_id)
        except UnknownUserException:
            logger.error(f"Could not find user with id {user_id}")
            return

        # Convert time from users' timezone to UTC
        caller_tz = pytz.timezone(user.tz)
        caller_datetime = caller_tz.localize(datetime.fromtimestamp(requested_timestamp))
        utc = caller_datetime.astimezone(pytz.utc)

        event_data = self._schedule_data.add_event(user_id, utc.timestamp(), title)
        event_cog = EventCog(self._bot, event_data, self.remove)
        self._event_cogs.append(event_cog)
        self._bot.add_cog(event_cog)

        # Save/write updated schedule to disk
        self._writer.write(self._schedule_data)

        # Start the countdown
        event_cog.start()

    def get_schedule(self) -> ScheduleData:
        return self._schedule_data

    def remove(self, event_cog: EventCog):
        """ Passed as a callback to `EventCog`'s so they can remove themselves when they trigger """

        self._schedule_data.remove(event_cog.event_data)
        self._writer.write(self._schedule_data)
        self._event_cogs.remove(event_cog)




class ListCommand(BaseCommand):

    def __init__(self, scheduler: Scheduler):
        super().__init__(
            alias="list",
            description="List all upcoming events",
            brief="List upcoming events",
            cog=scheduler,
        )
        self._scheduler: Scheduler = scheduler

    def execute(self, ctx: commands.Context, *args) -> discord.Embed:
        embed = embed_info("Upcoming events")
        for event in self._scheduler.get_schedule():
            embed.add_field(name=event.title,
                            value=datetime.fromtimestamp(event.time).strftime("%Y-%m-%d %H:%M"),
                            inline=False)
        return embed


class MeetCommand(BaseCommand):

    def __init__(self, scheduler: Scheduler):
        super().__init__(
            alias="meet",
            description="Schedule a meeting to trigger at a specified time",
            brief="Schedule a meeting",
            cog=scheduler,
        )
        self._scheduler: Scheduler = scheduler

    def execute(self, ctx: commands.Context, *args) -> discord.Embed:
        if len(args) < 3:
            return embed_failure("Not enough arguments")

        date = args[0]
        time = args[1]
        title = " ".join(args[2:])

        try:
            dt = datetime.strptime("{} {}".format(date, time), "%Y-%m-%d %H:%M")
        except ValueError:
            return embed_failure(title="Invalid date or time format",
                                 description="Date and time should be in the format YYYY-mm-dd HH:MM")

        try:
            self._scheduler.add_event(ctx.author.id, dt.timestamp(), title)
        except UnknownUserException:
            logger.warn(f"Unknown user with id {ctx.author.id}")
            return embed_failure(title="Failed to schedule event",
                                 description="Unknown user")

        return embed_success(title="Successfully scheduled event") \
            .add_field(name="Title", value=title) \
            .add_field(name="datetime", value=dt)
