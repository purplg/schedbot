import tempfile
from unittest import TestCase

from schedbot.data import UserEncoder, UserData, FileWriter


class TestUserData(TestCase):

    def test_add_user(self):
        user_data = UserData()
        user_data.add_user(123, "testUserId123", "Fake/Timezone")

        user = user_data._users[123]
        self.assertEqual(user.id, 123)
        self.assertEqual(user.name, "testUserId123")
        self.assertEqual(user.tz, "Fake/Timezone")

    def test_remove_user(self):
        user_data = UserData()
        user_data.add_user(123, "testUserId123", "Fake/Timezone")
        user_data.remove_user(123)

        self.assertDictEqual(user_data._users, {})
        self.assertDictEqual(user_data._users, {})

    def test_no_zone_duplicates(self):
        user_data = UserData()
        user_data.add_user(123, "testUserId123", "Fake/Timezone")
        user_data.add_user(456, "testUserId456", "Fake/Timezone")
        user_data.add_user(123, "testUserId123", "Other/Timezone2")
        user_data.add_user(456, "testUserId456", "Other/Timezone2")

        self.assertEqual(user_data._zones,
                         {'Other/Timezone2': [
                             {'id': 123, 'name': 'testUserId123', 'tz': 'Other/Timezone2'},
                             {'id': 456, 'name': 'testUserId456', 'tz': 'Other/Timezone2'}
                         ]})


class TestUserDataReadWriting(TestCase):

    def test_read_from_file(self):
        actual = b'[{"id": 23590704810, "name": "not a real user",   "tz": "America/Chicago"},' \
                 b' {"id": 12908349018, "name": "Another Fake User", "tz": "America/New_York"}]'
        expected = {
            23590704810: {'id': 23590704810, 'name': 'not a real user', 'tz': 'America/Chicago'},
            12908349018: {'id': 12908349018, 'name': 'Another Fake User', 'tz': 'America/New_York'}
        }
        with tempfile.NamedTemporaryFile() as tmp:
            tmp.write(actual)
            tmp.flush()
            user_data = UserData.new_from_file(tmp.name)
            self.assertEqual(user_data._users, expected)

    def test_write_to_file(self):
        user_data = UserData([
            {'id': 12389718923719, 'name': 'not a real user', 'tz': 'America/Chicago'},
            {'id': 34589231489010, 'name': 'Another Fake User', 'tz': 'America/New_York'}
        ])

        with tempfile.NamedTemporaryFile() as tmp:
            uw = FileWriter(tmp.name, UserEncoder)
            uw.write(user_data)
            expected = b'[{"id": 12389718923719, "name": "not a real user", "tz": "America/Chicago"}, ' \
                       b'{"id": 34589231489010, "name": "Another Fake User", "tz": "America/New_York"}]'
            self.assertEqual(tmp.read(), expected)
