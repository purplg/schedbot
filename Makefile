OPTS = --config ./conf/schedbot.yaml

run:
	./venv/bin/python bin/schedbot -v ${OPTS}

dryrun:
	./venv/bin/python bin/schedbot -d -v ${OPTS}

test:
	./venv/bin/python -m unittest tests/test_*.py

setup:
	/usr/bin/python3 -m venv venv
	./venv/bin/python -m pip install -r requirements.txt

clean:
	rm -r ./venv

