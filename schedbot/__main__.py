import sys

from schedbot.client import SchedBot
from schedbot.cog_scheduler import Scheduler
from schedbot.cog_timezone import Timezone
from schedbot.config import SchedbotConfig
from schedbot.log import logger
from schedbot.data import (
    UserData, UserEncoder,
    ScheduleData, ScheduleEncoder,
    FileWriter)


def main(schedbot_config: SchedbotConfig):
    logger.info("Initializing bot...")
    logger.debug(f"Configuration: {schedbot_config}")

    bot = SchedBot(command_prefix=schedbot_config['prefix'], channel_id=schedbot_config['channel_id'])

    user_writer = FileWriter(schedbot_config['users_file'], UserEncoder)
    user_data = UserData.new_from_file(schedbot_config['users_file'])
    timezone_cog = Timezone(bot, user_data, user_writer)

    sched_writer = FileWriter(schedbot_config['schedule_file'], ScheduleEncoder)
    schedule_data = ScheduleData.new_from_file(schedbot_config['schedule_file'])
    scheduler = Scheduler(bot, user_data, sched_writer, schedule_data)

    bot.add_cog(timezone_cog)
    bot.add_cog(scheduler)

    if not schedbot_config['dry_run']:
        bot.run(schedbot_config['token'])


if __name__ == "__main__":
    config = SchedbotConfig(sys.argv[1:])

    from schedbot.log import setup_logging

    setup_logging(verbose=config['verbose'])

    main(config)
