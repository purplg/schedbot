import discord

_color_success = 0x00ff00
_color_failure = 0xff0000
_color_info = 0xcccccc


def embed_success(title: str, description: str = None) -> discord.Embed:
    return _embed(title, _color_success, description)


def embed_failure(title: str, description: str = None) -> discord.Embed:
    return _embed(title, _color_failure, description)


def embed_info(title: str, description: str = None) -> discord.Embed:
    return _embed(title, _color_info, description)


def _embed(title: str, color: int, description: str = None) -> discord.Embed:
    embed = discord.Embed(color=color)
    embed.title = title
    if description is not None:
        embed.description = description
    return embed
