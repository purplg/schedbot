from datetime import datetime
from datetime import time


def mars_sol_date() -> float:
    tai_offset = 37
    millis = 1000 * datetime.now().timestamp()
    jd_ut = 2440587.5 + (millis / (8.64 * 10 ** 7))
    jd_tt = jd_ut + (tai_offset + 32.184) / 86400
    j2000 = jd_tt - 2451545.0
    msd = (((j2000 - 4.5) / 1.027491252) + 44796.0 - 0.00096)
    return msd


def mars_time() -> time:
    msd = mars_sol_date()
    mtc = (24 * msd) % 24
    mtc_hours = int(mtc)
    mtc_minutes = int((mtc - mtc_hours) * 60)
    # mtc_seconds = int(((mtc - mtc_hours)*60 - mtc_minutes)*60)
    mtc_time = datetime.now().replace(hour=mtc_hours, minute=mtc_minutes, second=0, microsecond=0).time()
    return mtc_time
