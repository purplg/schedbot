import json
import tempfile
from datetime import datetime
from unittest import TestCase
from freezegun.api import freeze_time

import pytz

from schedbot.client import SchedBot
from schedbot.cog_scheduler import Scheduler
from schedbot.cog_timezone import UserData
from schedbot.data import ScheduleData, ScheduleEncoder, FileWriter


class TestScheduleData(TestCase):

    def test_add_event_sorting(self):
        s = ScheduleData()
        s.add_event(79123, datetime.strptime("2019-05-05 03:44", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(79123, datetime.strptime("2019-03-26 16:27", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(79123, datetime.strptime("2020-01-18 08:46", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(79123, datetime.strptime("2020-02-29 17:59", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(79123, datetime.strptime("2019-05-28 17:32", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(79123, datetime.strptime("2019-11-14 23:21", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(79123, datetime.strptime("2020-09-08 11:05", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(79123, datetime.strptime("2019-01-26 21:52", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(79123, datetime.strptime("2019-07-09 12:42", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(79123, datetime.strptime("2020-10-14 20:03", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(79123, datetime.strptime("2019-07-28 15:16", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(79123, datetime.strptime("2020-04-02 03:15", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(79123, datetime.strptime("2020-11-14 23:21", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(79123, datetime.strptime("2020-09-25 22:14", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(79123, datetime.strptime("2019-08-02 12:29", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(79123, datetime.strptime("2020-12-13 15:14", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(79123, datetime.strptime("2020-03-24 16:37", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(79123, datetime.strptime("2020-09-12 23:50", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(79123, datetime.strptime("2019-08-07 22:18", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(79123, datetime.strptime("2019-05-26 22:24", "%Y-%m-%d %H:%M").timestamp(), "")

        self.assertEqual(s._events, [
            {'user_id': 79123, 'time': datetime.strptime("2019-01-26 21:52", "%Y-%m-%d %H:%M").timestamp(), 'title': ''},
            {'user_id': 79123, 'time': datetime.strptime("2019-03-26 16:27", "%Y-%m-%d %H:%M").timestamp(), 'title': ''},
            {'user_id': 79123, 'time': datetime.strptime("2019-05-05 03:44", "%Y-%m-%d %H:%M").timestamp(), 'title': ''},
            {'user_id': 79123, 'time': datetime.strptime("2019-05-26 22:24", "%Y-%m-%d %H:%M").timestamp(), 'title': ''},
            {'user_id': 79123, 'time': datetime.strptime("2019-05-28 17:32", "%Y-%m-%d %H:%M").timestamp(), 'title': ''},
            {'user_id': 79123, 'time': datetime.strptime("2019-07-09 12:42", "%Y-%m-%d %H:%M").timestamp(), 'title': ''},
            {'user_id': 79123, 'time': datetime.strptime("2019-07-28 15:16", "%Y-%m-%d %H:%M").timestamp(), 'title': ''},
            {'user_id': 79123, 'time': datetime.strptime("2019-08-02 12:29", "%Y-%m-%d %H:%M").timestamp(), 'title': ''},
            {'user_id': 79123, 'time': datetime.strptime("2019-08-07 22:18", "%Y-%m-%d %H:%M").timestamp(), 'title': ''},
            {'user_id': 79123, 'time': datetime.strptime("2019-11-14 23:21", "%Y-%m-%d %H:%M").timestamp(), 'title': ''},
            {'user_id': 79123, 'time': datetime.strptime("2020-01-18 08:46", "%Y-%m-%d %H:%M").timestamp(), 'title': ''},
            {'user_id': 79123, 'time': datetime.strptime("2020-02-29 17:59", "%Y-%m-%d %H:%M").timestamp(), 'title': ''},
            {'user_id': 79123, 'time': datetime.strptime("2020-03-24 16:37", "%Y-%m-%d %H:%M").timestamp(), 'title': ''},
            {'user_id': 79123, 'time': datetime.strptime("2020-04-02 03:15", "%Y-%m-%d %H:%M").timestamp(), 'title': ''},
            {'user_id': 79123, 'time': datetime.strptime("2020-09-08 11:05", "%Y-%m-%d %H:%M").timestamp(), 'title': ''},
            {'user_id': 79123, 'time': datetime.strptime("2020-09-12 23:50", "%Y-%m-%d %H:%M").timestamp(), 'title': ''},
            {'user_id': 79123, 'time': datetime.strptime("2020-09-25 22:14", "%Y-%m-%d %H:%M").timestamp(), 'title': ''},
            {'user_id': 79123, 'time': datetime.strptime("2020-10-14 20:03", "%Y-%m-%d %H:%M").timestamp(), 'title': ''},
            {'user_id': 79123, 'time': datetime.strptime("2020-11-14 23:21", "%Y-%m-%d %H:%M").timestamp(), 'title': ''},
            {'user_id': 79123, 'time': datetime.strptime("2020-12-13 15:14", "%Y-%m-%d %H:%M").timestamp(), 'title': ''}])

    def test_get_next(self):
        s = ScheduleData()
        s.add_event(58123, datetime.strptime("2019-09-08 12:10", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(58123, datetime.strptime("2020-06-29 22:37", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(58123, datetime.strptime("2019-12-29 13:20", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(58123, datetime.strptime("2019-10-29 12:58", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(58123, datetime.strptime("2019-02-22 18:26", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(58123, datetime.strptime("2020-01-01 15:17", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(58123, datetime.strptime("2020-10-02 20:35", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(58123, datetime.strptime("2020-07-23 19:42", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(58123, datetime.strptime("2020-01-12 13:39", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(58123, datetime.strptime("2019-11-19 18:07", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(58123, datetime.strptime("2019-10-22 14:02", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(58123, datetime.strptime("2020-08-20 23:09", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(58123, datetime.strptime("2019-07-13 11:27", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(58123, datetime.strptime("2020-05-04 15:43", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(58123, datetime.strptime("2020-02-15 15:48", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(58123, datetime.strptime("2020-07-08 11:44", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(58123, datetime.strptime("2020-04-27 13:33", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(58123, datetime.strptime("2019-01-02 12:50", "%Y-%m-%d %H:%M").timestamp(), "")  # Earliest event
        s.add_event(58123, datetime.strptime("2019-05-10 21:35", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(58123, datetime.strptime("2019-12-21 16:19", "%Y-%m-%d %H:%M").timestamp(), "")

        self.assertEqual(s.peek().time, datetime.strptime("2019-01-02 12:50", "%Y-%m-%d %H:%M").timestamp())

    def test_pop(self):
        s = ScheduleData()
        s.add_event(145890, datetime.strptime("2020-12-11 22:23", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(145890, datetime.strptime("2020-07-23 08:49", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(145890, datetime.strptime("2019-09-25 19:48", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(145890, datetime.strptime("2020-08-09 11:21", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(145890, datetime.strptime("2020-05-01 20:43", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(145890, datetime.strptime("2020-08-04 06:25", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(145890, datetime.strptime("2019-04-02 01:49", "%Y-%m-%d %H:%M").timestamp(), "")  # Earliest event
        s.add_event(145890, datetime.strptime("2020-06-30 13:49", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(145890, datetime.strptime("2019-06-09 00:45", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(145890, datetime.strptime("2019-12-29 04:37", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(145890, datetime.strptime("2020-06-27 13:37", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(145890, datetime.strptime("2020-02-05 03:34", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(145890, datetime.strptime("2020-11-03 22:01", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(145890, datetime.strptime("2019-08-24 08:49", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(145890, datetime.strptime("2020-01-17 17:27", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(145890, datetime.strptime("2020-08-18 22:00", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(145890, datetime.strptime("2020-02-24 21:05", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(145890, datetime.strptime("2019-06-15 14:00", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(145890, datetime.strptime("2019-10-15 18:45", "%Y-%m-%d %H:%M").timestamp(), "")
        s.add_event(145890, datetime.strptime("2020-05-28 12:04", "%Y-%m-%d %H:%M").timestamp(), "")

        self.assertEqual(s.pop().time, datetime.strptime("2019-04-02 01:49", "%Y-%m-%d %H:%M").timestamp())
        self.assertEqual(len(s._events), 20)

    def test_writing(self):
        with tempfile.NamedTemporaryFile() as tmp:
            schedule_writer = FileWriter(tmp.name, ScheduleEncoder)
            schedule_data = ScheduleData()
            schedule_data.add_event(51890, 1557288540.0, 'This is a test event title')
            schedule_data.add_event(51890, 1552288080.0, 'Second event title, should come first')
            schedule_writer.write(schedule_data)
            tmp.flush()
            tmp.seek(0)
            result = tmp.read()
            expected = b'[{"user_id": 51890, "time": 1552288080.0, "title": "Second event title, should come first"},' \
                       b' {"user_id": 51890, "time": 1557288540.0, "title": "This is a test event title"}]'
            self.assertEqual(result, expected)

    def test_reading(self):
        with tempfile.NamedTemporaryFile() as tmp:
            tmp.write(b'[{"user_id": 18903, "time": 1550341080.0, "title": "Test title for read testing"},'
                      b' {"user_id": 18903, "time": 1604951340.0, "title": "Other title for read testing"}]')
            tmp.flush()
            tmp.seek(0)
            schedule_data = ScheduleData.new_from_file(tmp.name)

        expected = '[{"user_id": 18903, "time": 1550341080.0, "title": "Test title for read testing"},' \
                   ' {"user_id": 18903, "time": 1604951340.0, "title": "Other title for read testing"}]'
        self.assertEqual(json.dumps(schedule_data._events), expected)


@freeze_time(datetime(year=2024, month=5, day=25, hour=4, minute=14, tzinfo=pytz.utc))
class TestScheduleCog(TestCase):

    def test_write_on_add(self):
        with tempfile.NamedTemporaryFile() as tmp:
            user_data = UserData([{
                'id': 123,
                'name': 'User123',
                'tz': 'UTC',
            }])
            writer = FileWriter(tmp.name, ScheduleEncoder)
            schedule_data = ScheduleData([])
            scheduler = Scheduler(SchedBot(0, '!'), user_data, writer, schedule_data)
            scheduler.add_event(123, datetime(year=2025, month=2, day=13, hour=0, minute=37, tzinfo=pytz.utc).timestamp(),
                                "This is the title")
            self.assertEqual(tmp.read(), b'[{"user_id": 123, "time": 1739407020.0, "title": "This is the title"}]')

    def test_write_on_remove(self):
        with tempfile.NamedTemporaryFile() as tmp:
            user_data = UserData([{
                'id': 123,
                'name': 'User123',
                'tz': 'UTC',
            }])
            writer = FileWriter(tmp.name, ScheduleEncoder)
            schedule_data = ScheduleData([{
                'user_id': 123,
                'time': datetime(year=2025, month=2, day=13, hour=0, minute=37).timestamp(),
                'title': "This is the title"
            }])
            scheduler = Scheduler(SchedBot(0, '!'), user_data, writer, schedule_data)
            scheduler.remove(scheduler._event_cogs[0])
            self.assertEqual(tmp.read(), b'[]')
