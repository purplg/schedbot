from datetime import datetime

import discord
import pytz
from Levenshtein import distance as levenshtein
from discord.ext import commands

from schedbot.client import SchedBot
from schedbot.cmd_base import BaseCommand
from schedbot.cog_base import BaseCog
from schedbot.embeds import embed_failure, embed_success
from schedbot.mars import mars_time
from schedbot.data import UserData, FileWriter


class Timezone(BaseCog):

    def __init__(self, bot: SchedBot, user_data: UserData, writer: FileWriter):
        self._bot = bot
        self._writer = writer
        self._users = user_data

    def after_added(self):
        self._bot.add_command(SetCommand(self._users, self._writer))
        self._bot.add_command(WhenCommand(self._users))


class WhenCommand(BaseCommand):

    def __init__(self, user_data: UserData):
        super().__init__(
            alias="when",
            description="Find out what time it is in other timezones. If you omit the _time_ argument, current local "
                        "time will be used.",
            brief="Everyone's time",
            usage="[_time_]"
        )
        self.user_data = user_data

    def execute(self, ctx: commands.Context, *args) -> discord.Embed:
        user = self.user_data.get_user(ctx.author.id)
        if user is None:
            return embed_failure("You must set your timezone with `!set` first")

        caller_tz = pytz.timezone(user.tz)

        # Check if caller is asking for the current time or a specific time
        if len(args) > 0:
            query = datetime.today().strftime('%Y-%m-%d {}').format(args[0])
            try:
                queried_time = datetime.strptime(query, '%Y-%m-%d %H:%M')
            except ValueError:
                return embed_failure("Invalid time format")
            queried_time = caller_tz.localize(queried_time)
        else:
            queried_time = datetime.now(caller_tz)

        embed = embed_success("When is everyone?")
        timezones = self.user_data.get_timezones()
        for tzname in timezones:
            users_in_tz = self.user_data.get_users_in_timezone(tzname)
            if len(users_in_tz) == 0:
                continue

            tz = pytz.timezone(tzname)
            user_list = ", ".join([user.name for user in users_in_tz])
            time = queried_time.astimezone(tz)
            embed.add_field(name=user_list,
                            value=time.strftime("%a %H:%M {}").format(time.tzname()),
                            inline=False)

        # Append Marvin's Martian time
        mtc = mars_time().strftime("%H:%M MTC")
        embed.add_field(name="Marvin", value=mtc)

        return embed.set_footer(text="Your time: {}".format(queried_time.strftime("%H:%M %Z")))


class SetCommand(BaseCommand):
    """ A bot command for mapping Discord users to their respective timezones """

    def __init__(self, user_data: UserData, user_writer: FileWriter):
        super().__init__(
            alias="set",
            description="Register your Discord account to the specified timezone",
            brief="Set your timezone",
            usage="_timezone_"
        )
        self.user_data = user_data
        self.user_writer = user_writer

    def execute(self, ctx: commands.Context, *args) -> discord.Embed:
        if len(args) == 0:
            return self._failure_embed("None")

        try:
            tz = pytz.timezone(args[0])
        except pytz.UnknownTimeZoneError:
            similar_tz = self._closest_timezone_name(args[0])
            return self._failure_embed("Unknown timezone. Did you mean \"{}\"?".format(similar_tz))

        try:
            self.user_data.add_user(ctx.author.id, ctx.author.name, tz.zone)
            self.user_writer.write(self.user_data)
            return self._success_embed(ctx.author.name, tz.zone)
        except:
            return self._failure_embed("Failed to add user")

    @staticmethod
    def _closest_timezone_name(tz_name) -> str:
        lowest_score = float('inf')
        possibilities = []
        for tz in pytz.all_timezones:
            tz_score = levenshtein(tz_name, tz)
            if tz_score < lowest_score:
                possibilities = [tz]
                lowest_score = tz_score
            elif tz_score == lowest_score:
                possibilities.append(tz)
        return possibilities[0]

    @staticmethod
    def _failure_embed(message: str) -> discord.Embed:
        return embed_failure(title="Registration failed! :(", description=message)

    @staticmethod
    def _success_embed(user_name, timezone) -> discord.Embed:
        return embed_success("Registration successful! :)") \
            .add_field(name="User", value=user_name) \
            .add_field(name="Timezone", value=timezone)
