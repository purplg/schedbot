import tempfile
from datetime import datetime
from unittest import TestCase

from freezegun.api import freeze_time
import pytz

from schedbot.client import SchedBot
from schedbot.cog_scheduler import ListCommand, Scheduler
from schedbot.cog_timezone import UserData
from schedbot.data import ScheduleEncoder, FileWriter


@freeze_time(datetime(year=2024, month=5, day=25, hour=4, minute=14, tzinfo=pytz.utc))
class TestListCommand(TestCase):

    def test_list(self):
        with tempfile.NamedTemporaryFile() as tmp:
            user_data = UserData([{'id': 34786, 'name': 'UserWithId34786', 'tz': 'UTC'}, ])

            schedule = Scheduler(SchedBot(0, '!'), user_data, FileWriter(tmp.name, ScheduleEncoder))

            # datetime(year=2024, month=5, day=25, hour=6, minute=14, tzinfo=pytz.utc).timestamp()
            schedule.add_event(34786, 1716617640.0, "Test event title")

            # datetime(year=2024, month=5, day=25, hour=5, minute=14, tzinfo=pytz.utc).timestamp()
            schedule.add_event(34786, 1716614040.0, "Second event title should be first")

            # datetime(year=2024, month=5, day=25, hour=7, minute=14, tzinfo=pytz.utc).timestamp()
            schedule.add_event(34786, 1716621240.0, "Aaannnddd... the 3rd")

            cmd = ListCommand(schedule)
            embed = cmd.execute(None)  # type: ignore

            self.assertEqual(embed.title, "Upcoming events")
            self.assertEqual(embed.fields[0].name, "Second event title should be first")
            self.assertEqual(embed.fields[0].value, "2024-05-25 05:14")
            self.assertEqual(embed.fields[1].name, "Test event title")
            self.assertEqual(embed.fields[1].value, "2024-05-25 06:14")
            self.assertEqual(embed.fields[2].name, "Aaannnddd... the 3rd")
            self.assertEqual(embed.fields[2].value, "2024-05-25 07:14")
