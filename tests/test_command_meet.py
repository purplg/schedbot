import tempfile
from unittest import TestCase
from datetime import datetime

from freezegun.api import freeze_time
import pytz

from schedbot.client import SchedBot
from schedbot.cog_scheduler import MeetCommand
from schedbot.cog_scheduler import Scheduler
from schedbot.cog_timezone import UserData
from schedbot.data import ScheduleEncoder, FileWriter


class MockAuthor:
    def __init__(self, id: int, name: str):
        self.id = id
        self.name = name


class MockContext:
    def __init__(self, author_id: int, author_name: str):
        self.author = MockAuthor(author_id, author_name)


@freeze_time(datetime(year=2024, month=5, day=25, hour=4, minute=14, tzinfo=pytz.utc))
class TestMeetCommand(TestCase):

    def test_meet_not_enough_arguments(self):
        with tempfile.NamedTemporaryFile() as tmp:
            bot = SchedBot(0, '!')
            scheduler_writer = FileWriter(tmp.name, ScheduleEncoder)
            scheduler = Scheduler(bot, UserData([{'id': 61289, 'name': 'UserWithId61289', 'tz': 'UTC'}, ]), scheduler_writer)
            cmd = MeetCommand(scheduler)
            embed = cmd.execute(MockContext(36294, 'UserWithId36294'), "", "")  # type: ignore
            self.assertEqual(embed.title, "Not enough arguments")

    def test_meet_invalid_datetime_format(self):
        with tempfile.NamedTemporaryFile() as tmp:
            bot = SchedBot(0, '!')
            scheduler_writer = FileWriter(tmp.name, ScheduleEncoder)
            scheduler = Scheduler(bot, UserData([{'id': 94562, 'name': 'UserWithId94562', 'tz': 'UTC'}, ]), scheduler_writer)
            cmd = MeetCommand(scheduler)
            embed = cmd.execute(MockContext(94562, 'UserWithId94562'), "2020-22-13", "02:10", "")  # type: ignore
            self.assertEqual(embed.title, "Invalid date or time format")

    def test_meet_execute(self):
        with tempfile.NamedTemporaryFile() as tmp:
            bot = SchedBot(0, '!')
            scheduler_writer = FileWriter(tmp.name, ScheduleEncoder)
            scheduler = Scheduler(bot, UserData([{'id': 61289, 'name': 'UserWithId61289', 'tz': 'UTC'}, ]), scheduler_writer)
            cmd = MeetCommand(scheduler)
            cmd.execute(MockContext(61289, 'UserWithId61289'), "2025-02-13", "02:13", "title", "of", "text", "stuff")  # type: ignore
            self.assertEqual(scheduler.get_schedule()._events, [{'user_id': 61289, 'time': 1739412780.0, 'title': "title of text stuff"}])
