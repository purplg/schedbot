from abc import abstractmethod
from typing import Optional

import discord
from discord.ext import commands

from schedbot.embeds import embed_failure
from schedbot.log import logger


class BaseCommand(commands.Command):

    def __init__(self, alias: str, description: str, usage: Optional[str] = None, **kwargs):
        super().__init__(
            name=alias,
            description=description,
            usage=usage,
            func=self.command,
            **kwargs)

    async def command(self, ctx, *args):
        logger.debug(
            f"Executing command '{self.qualified_name}' from message '{ctx.message.content}' by user '{ctx.message.author.name}'")
        try:
            await ctx.send(embed=self.execute(ctx, *args))
        except:
            logger.error(f"An error occurred while executing the command: '{self.qualified_name}' "
                         f"invoked by message '{ctx.message.content}' "
                         f"by user '{ctx.message.author.name}'")
            await ctx.send(embed=embed_failure("An unknown error occurred, wtf."))

    @abstractmethod
    def execute(self, ctx: commands.Context, *args) -> discord.Embed:
        pass
